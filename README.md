Layout Builder for UGT
======================

This is a usefull HTML template to quickly build web views. This repository has an "original" and a "custom" versions so that you can work on one and use the other as a referrence in case you get lost.

# Install
*Note:* Always use Windows PowerShell or Linux Terminal, Bash if possible.

1. Go to a working folder and clone this repository: `git clone https://bitbucket.org/starkwolf/t17003_ugt_tp1.git`.
1. Run `npm install -g bower`.
1. Go to `./original/master` or `./custom/master`.
1. Run `npm install`.
1. Run `bower install`.
1. Run `gulp`.
1. Run `npm install -g http-server`.
1. Go to `./original/` or `./custom/`.
1. Run `http-server . -a 127.0.0.1 -p 8080`.
1. Navigte to `https://localhost:8081`.

Gulp will keep running and every change you make inside `./master` will be automatically rebuilt to `./app`.

## Sidebar
Items on the sidebar can be customized modifying `./master/sidebar.json`. The sidebar is present on every HTML file. In spite you've updated `./master/sidebar.json`, the sidebar won't be updated on the HTML files until you stop Gulp, start it again, and modify the JADE files in which you wan to update the sidebar.

## JADE
JADE is now called Pug but don't worry about that, it will compile anyway.

It's pretty intuitive. It works by rendering HTML elements according to the CSS Selectors you use on the `jade` file. You can nest elements inside each other by indenting with tabs. There is at least one JADE file per HTML file. But there are more JADEs than HTMLs file because some JADEs include other JADEs.

This:

```pug
.header
	img.logo(src="img/image001.jpg")
	ul
		li.nav-item Item 1
		li.nav-item	Item 2
```

Compiles to this:

```
<div class="header">
	<img class="logo" src="img/image001.jpg"/>
	<ul>
		<li>Item 1</li>
		<li>Item 2</li>
	</ul>
```

Note: Icons are added using classes on elements. Find them here: [http://simplelineicons.com/](http://simplelineicons.com/).

## JS
Every modification you make inside `./master/js` will be built into one file which is `./app/js/app.js`.